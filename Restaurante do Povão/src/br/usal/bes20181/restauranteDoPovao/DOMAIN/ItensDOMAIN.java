package br.usal.bes20181.restauranteDoPovao.DOMAIN;

public class ItensDOMAIN {

	private int codigo;
	private String nome;
	private String descricao;
	private Double valorUnitario;
	
	public ItensDOMAIN(int codigo, String nome, String descricao, Double valorUnitario) {
		this.codigo = codigo;
		this.nome = nome;
		this.descricao = descricao;
		this.valorUnitario = valorUnitario;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public Double getValorUnitario() {
		return valorUnitario;
	}
	
	
}