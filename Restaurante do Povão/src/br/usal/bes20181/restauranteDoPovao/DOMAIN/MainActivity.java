package br.usal.bes20181.restauranteDoPovao.DOMAIN;

import br.usal.bes20181.restauranteDoPovao.TUI.MesaTUI;

public class MainActivity {

	public static void main(String args[]) {
		MesaTUI.RegistrarMesa();
		MesaTUI.MostrarMesa();
	}
}