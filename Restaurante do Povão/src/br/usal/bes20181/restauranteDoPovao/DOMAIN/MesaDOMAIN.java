package br.usal.bes20181.restauranteDoPovao.DOMAIN;

public class MesaDOMAIN {

	private int numero;
	private int capacidadePessoas;
	private String situacaoMesa;

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getCapacidadePessoas() {
		return capacidadePessoas;
	}

	public void setCapacidadePessoas(int capacidadePessoas) {
		this.capacidadePessoas = capacidadePessoas;
	}

	public String getSituacaoMesa() {
		return situacaoMesa;
	}

	public void setSituacaoMesa(String situacaoMesa) {
		this.situacaoMesa = situacaoMesa;
	}

}