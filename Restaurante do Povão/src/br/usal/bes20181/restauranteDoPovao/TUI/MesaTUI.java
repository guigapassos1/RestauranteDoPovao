package br.usal.bes20181.restauranteDoPovao.TUI;

import java.util.Scanner;

import br.usal.bes20181.restauranteDoPovao.DOMAIN.MesaDOMAIN;

public class MesaTUI {

	static Scanner sc = new Scanner(System.in);
	static MesaDOMAIN mesaDOMAIN = new MesaDOMAIN();

	public static void RegistrarMesa() {

		System.out.println("Bem vindo ao Restaurante do Pov�o");
		System.out.println("Escolha o n�mero da mesa:");
		mesaDOMAIN.setNumero(sc.nextInt());
		System.out.println("Quantas pessoas:");
		mesaDOMAIN.setCapacidadePessoas(sc.nextInt());
		mesaDOMAIN.setSituacaoMesa("OCUPADA");

	}
	public static void MostrarMesa() {
		System.out.println("-----MESA-----");
		System.out.println("N�mero: " + mesaDOMAIN.getNumero());
		System.out.println("Quantidade de pessoas: " + mesaDOMAIN.getCapacidadePessoas());
		System.out.println("Situa��o: " + mesaDOMAIN.getSituacaoMesa());
		System.out.println("-----MESA-----\n");
	}

}